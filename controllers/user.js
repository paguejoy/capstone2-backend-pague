const User = require('../models/user');
const Course = require('./../models/course');
const bcrypt = require('bcrypt');
const { createAccessToken } = require('./../auth');

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		// 10 = salt/string of characters added to the password before hashing
		// "juan1234abcdefghij" = "fh2lkj3hlj2h3t"
		password: bcrypt.hashSync(params.password, 10)
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
	})
}

module.exports.login = (req,res) => {
	// email is correct but the password is not correct
	// email and password are not correct
	// email and password are correct
	// email is correct but password is not correct
	User.findOne({email : req.body.email})
	.then( user => {
		if( !user ) { 
			res.send(false)
		} else {
			// res.send(user)
			let comparePasswordResult = bcrypt.compareSync(req.body.password, user.password); 
			if (!comparePasswordResult) {
				res.send(false)
			} else {
				
				res.send({accessToken : createAccessToken(user)})
			}
		}
	}).catch( err => {
		res.status(500).send("Server Error")
	})

}

module.exports.get = (params) => {
	return User.findById(params.userId).select({ password: 0}).then( user => {
		return user
	})
}

module.exports.enroll = params => {

// 	return User.findById(params.userId).then( user => {
// 		user.enrollments.push( {courseId: params.courseId})
// 		return user.save().then((user) => {
// 			return Course.findById(params.courseId).then( course => {
// 				course.enrollees.push({userId: params.userId})
// 				return course.save().then( course => {
// 					return course ? true: false
// 				})
// 			})
// 		})
// 	})
// }

  return User.findById(params.userId).then((user) => {
    const foundEnrollment = user.enrollments.some((enrollment) => {
      return enrollment.courseId === params.courseId;
    });

    if (!foundEnrollment) {
      user.enrollments.push({ courseId: params.courseId });
      return user.save().then((user) => {
        return Course.findById(params.courseId).then((course) => {
          course.enrollees.push({ userId: params.userId });
          return course.save().then((course) => {
            return course ? true : false;
          });
        });
      });
    } else {
      return "Course is already enrolled";
    }
  });
};

module.exports.editProfile = params => {
	let {id, firstName, lastName, email, mobileNo, password} = params;

	if (password !== undefined) {
		let password = bcrypt.hashSync(params.password,10)

		return User.findByIdAndUpdate(id, { firstName, lastName, email, mobileNo, password},
			//{ 
			// new : true,
			// useFindAndModify: false - moved to app.js
			//}
		)
		.then( (doc, err) => {
			return err ? false : true
		})
	}else {
		return User.findByIdAndUpdate(id, { firstName, lastName, email, mobileNo}, 
			//{ 
			// new : true,
			// useFindAndModify: false - moved to app.js
			//}
		)
		.then( (doc, err) => {
			return err ? false : true
		})
	}

}


// module.exports.sample = () => {

// 	let dataSample = {
// 		response: "hello, this is from the server."
// 	}

// 	let data = new Promise((res,rej) => {

// 		res(dataSample)
// 		rej(error)


// 	})

// 	return data

// }

module.exports.sample = async () => {

const allUsers = await User.find();

return allUsers
}